class Tweet < ApplicationRecord
    belongs_to :user
    has_many :comments, dependent: :destroy
    mount_uploader :picture, PictureUploader
    validates :comment, :picture, presence: true
end

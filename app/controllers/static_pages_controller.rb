class StaticPagesController < ApplicationController
  include SessionsHelper
  
  def home
    if logged_in?
      @tweets  = current_user.tweets.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end
  
  def about
  end
  
end

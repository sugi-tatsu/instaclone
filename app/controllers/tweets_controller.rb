class TweetsController < ApplicationController
  include SessionsHelper
    
  def new
    @tweet=Tweet.new
  end
  
  def show
    @tweet=Tweet.find_by(id: params[:id])
    @comments=@tweet.comments
    @comment=Comment.new
  end
   
  def create
    @tweet=current_user.tweets.build(tweet_params)
    if @tweet.save
      flash[:notice] = "投稿しました"
      redirect_to tweet_path(@tweet)
    else
       @feed_items = []
      render 'new'
    end
  end
    
  private
  
  def tweet_params
    params.require(:tweet).permit(:picture,:comment)
  end
end

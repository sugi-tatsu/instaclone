class CommentsController < ApplicationController
    
    def create
         @tweet=Tweet.find_by(id: params[:tweet_id])
         @comment=Comment.new(comment_params)
         @comment.user_id=current_user.id
         @comment.tweet=@tweet 
      if @comment.save
        flash[:notice] = "投稿しました"
      else
        Rails.logger.error @comment.errors.full_messages.join('')
        flash[:alert] = "投稿できませんでした"
      end
        redirect_to tweet_path(@tweet), method: :get
    end
    
    def destroy
      @comment=Comment.find_by(id: params[:tweet_id]).destroy
      flash[:notice] = "削除しました"
      redirect_to tweet_path
    end
    
    private
  def comment_params
    params.require(:comment).permit(:text,:user_id, :tweet_id)
  end
  
end

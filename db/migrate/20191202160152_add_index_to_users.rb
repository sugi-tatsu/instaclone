class AddIndexToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :website, :text
    add_column :users, :introduction, :text
    add_column :users, :tel, :text
  end
end
